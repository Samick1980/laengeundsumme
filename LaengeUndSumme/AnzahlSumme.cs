﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung
{
    public class AnzahlSumme
    {
        public static int[] CountPositivesSumNegatives(int[] input)
        {
            int positiv = 0;
            int negativ = 0;
            int[] ergebnis = new int[2];
            
            foreach (int zahl in input)
            {
                if (zahl > 0)
                { 
                    positiv = positiv + 1; 
                }
                else 
                { 
                    negativ = negativ + zahl; 
                }
            }

            ergebnis[0] = positiv;
            ergebnis[1] = negativ;
            return ergebnis; //return an array with count of positives and sum of negatives
        }
    }
}
